#include "Solution.h"
#include "Display.h"

#include <SDL2/SDL.h>
#include <iostream>

int main(int argc, char *argv[])
{
	if (argc != 2){
		std::cerr<< "usage: overcooked [solutionFile]" << std::endl;
		return -1;
	}

	std::string pathToExe = argv[0];
	std::size_t lastSlash = pathToExe.find_last_of('/');
	if (lastSlash == std::string::npos){
		pathToExe = "";
	} else {
		pathToExe = pathToExe.substr(0, lastSlash+1);
	}

	Solution solution(argv[1]);
	if (!solution.load()) {
		std::cerr << "Failed to load solution" << std::endl;
		return -1;
	}


	Display display(&solution, pathToExe);
	if (!display.init("Overcooked Animation")) {
		std::cerr << "Failed to initialize display" << std::endl;
		return -1;
	}



	bool running = true;
	unsigned long lastRender = 0;
	while (running) {
		display.handleEvents(running);  // Handle events like play, pause, quit
		if (clock() - lastRender > CLOCKS_PER_SEC / 60){
			//unsigned long startRender = clock();
			display.render();	   // Render the current frame
			//std::cerr<<"renderTime "<<(clock()-startRender)*1000./CLOCKS_PER_SEC<<"ms"<<std::endl;
		} else {
			SDL_Delay(5);
		}
	}

	display.cleanup();
	
	return 0;
}
