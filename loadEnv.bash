#!/bin/bash


# CURRENT=$(pwd)
# cd /home/tgateau/git/saclab/ssa/intro-htn
# source loadEnv.bash 
## source ~/git/saclab/ssa/intro-htn/loadEnv.bash 
# echo $HTN_PATH

HTN_PATH=$(pwd)

#HyperTensioN execution command: 
function plan {
ruby ${HTN_PATH}/HyperTensioN/Hype.rb $1 $2 $3
}
alias plan='plan'

#Ruby dot:
function planviz {
plan $1 $2 dot
dot -Tpdf "$1".dot > "$1".pdf && 
evince "$1".pdf &
}
alias planviz='planviz'

echo "usage: "
echo "plan domain.hddl problem.hddl debug"
echo "planviz domain.hddl problem.hddl debug"
