# TP01 - Practice - Introduction to HTN planning

[TP 01 instructions](https://gitlab.isae-supaero.fr/saclab/ssa/intro-htn/-/tree/main/TP01)

# TP02 - Cooking simulation

[TP 02 instructions](https://gitlab.isae-supaero.fr/saclab/ssa/intro-htn/-/tree/main/TP02)

# TP03 - Planning and execution

[TP 03 instructions](https://gitlab.isae-supaero.fr/saclab/ssa/intro-htn/-/tree/main/TP03)

# Load Julia in SI machines

```
cd $HTN_PATH
export MODULEPATH=$HTN_PATH:$MODULEPATH
module load julia
```


# Useful commands

```
mkdir -p git && cd git
git clone https://gitlab.isae-supaero.fr/saclab/ssa/intro-htn.git
cd intro-htn
HTN_PATH=$(pwd)

loadEnv.bash

cd $HTN_PATH
module load julia/1.9.3 
module load anaconda3/2023
```

## Domain visualization

```
planviz travelWalkTaxi.hddl gotoPark_problem.hddl 
```

## Planner execution (with debug)

```
plan travelWalkTaxi.hddl gotoPark_problem.hddl debug
```


