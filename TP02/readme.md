
# TP02 - Cooking simulation



# Classical planning application in an "Overcook" style game.


![cookingSim.png](cookingSim.png "PDDL")


## Domain

Planner will decide which actions the agent will achieve to cook onion soups. Goal is to deliver as much as onion soups to the customers waiting.

Actions are already defined for the agent: 
 - move (from one location to another one)
 - pickup (an ingredient)
 - drop (an ingredient)
 - pickup-stack (an ingredient from a stack of ingredients)
 - drop-stack (an ingredient on a stack)
 - chop (a specific ingredient: onions)
 - cook (a specific recipie: onion soup)
 - serve (an ingredient that is ready to be served)
  
Additional actions are provided, mainly for debug purpose: 
 - get_position (of an agent)
 - checkLoc (of an ingredient)
 - stop
 - idle
 - checkCarry (what ingredient my agent is carrying?)

Partially filled file is provided (file: cooking_domain.hddl).

## Problem

To begin with, we will consider only one agent a1. 

A world is already set (file: cooking_problem.hddl). Predicates are specifying objects caracteristics. 

Locations are connected between them to simplify agent movement in the  world (e.g. the center of the map is connected to the cooking place: "connected centerloc cookLoc").

Eventually, some soup recipies are provided: a lite (with only 1 chopped onion), esay one, and a standard one (with 3 chopped onions).

## Set-up project environment

  - [x]  if you ahve completed TP01, repository should be up to date, else

```
mkdir -p git && cd git
git clone https://gitlab.isae-supaero.fr/saclab/ssa/intro-htn.git
cd intro-htn
HTN_PATH=$(pwd)
```

### On SI computers

- [ ] if needed re-load SI environment
- [ ] check that the planner is working

```
cd $HTN_PATH
module load julia/1.9.3 
module load anaconda3/2024
source loadEnv.bash
cd TP02/
plan cooking_domain.hddl cooking_problem.hddl debug > plan.txt
```

## check the simulator

 - [ ] check that the simulator is working

```
cd OvercookedVisu
make
cd ..
./OvercookedVisu/overcooked ../plan.txt
```

## Repository Overview

```
plan cooking_domain.hddl cooking_problem.hddl
```

open 2 files: 
 - cooking_domain.hddl  (the panning domain)
 - cooking_problem.hddl (the planning problem)


  - [x]  an example of problem is provided - serving one soup to your customers
  - [ ] check that you can generate the plan and load it into the simulator

```
plan cooking_domain.hddl cooking_problem.hddl debug > plan.txt
./OvercookedVisu/overcooked plan.txt
```

  - [ ] now, comment 1 line and uncomment 1 line to make the agent start "far away" from the delivery location and check that planning process will fail.

  - [ ]  an example of domain is provided - your goal is to complete abstract tasks : 
    - DO NOT change elementary actions
    - DO NOT change predicates or types
    - you can create as many tasks (and associated methods) as you wish to provide 3 onion soups to you customers.
    - check execution line per line... HDDL is not the more ergonomic way to debug... use recursivity with care.


## recommendations

 - Start to implement move_to / goto tasks
 - use (yes) method to validate your current behavior
 - (if necessary, remove tasks and preconditions for testing purposes...)
 - proceed step by step, line per line, varaible per variable...

## problem resolution suggestion

 - when movement it's ok, test if you are able to deliver a "soupLite" located at the "cookLoc" location (comment / uncomment problem file)
 - try to cook a "soupLite" recipie and deliver it (comment / uncomment problem file)
 - try to chop 1 onion, cook the chopped onion into a soupLite and deliver it
 - eventually, cook a 3 onion soup and serve it.


## Open question: 

Find a way to provide as many onion as possible in the "stockloc" to do an infinite number of soups.
