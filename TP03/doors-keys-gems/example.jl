# Test gridworld rendering

# include("/home/tgateau/git/t.gateau/biblio/HTN/exercices/doors-keys-gems/testTG.jl")

using PDDL, SymbolicPlanners, PlanningDomains
using GLMakie
using PDDLViz

#import PDDLViz
# Load example gridworld domain and problem
#domain = load_domain(:doors_keys_gems)
#problem = load_problem(:doors_keys_gems, 1)


domain = load_domain("domain.pddl")
problem = load_problem("problem-1.pddl")
#problem = load_problem("/home/tgateau/git/t.gateau/biblio/HTN/exercices/doors-keys-gems/problem-7.pddl")

# Load array extension to PDDL
PDDL.Arrays.register!()

# Construct initial state from domain and problem
state = initstate(domain, problem)

# Construct gridworld renderer
gem_colors = PDDLViz.colorschemes[:vibrant]
renderer = PDDLViz.GridworldRenderer(
    resolution = (600, 700),
    agent_renderer = (d, s) -> RobotGraphic(color=:black),    #HumanGraphic
    obj_renderers = Dict(
        :key => (d, s, o) -> KeyGraphic(  # KeyGraphic
            visible=!s[Compound(:has, [o])]
        ),
        :door => (d, s, o) -> LockedDoorGraphic(
            visible=s[Compound(:locked, [o])]
        ),
        :gem => (d, s, o) -> GemGraphic(
            visible=!s[Compound(:has, [o])],
            color=gem_colors[parse(Int, string(o.name)[end])]
        )
    ),
    show_inventory = true,
    inventory_fns = [(d, s, o) -> s[Compound(:has, [o])]],
    inventory_types = [:item]
)

# Render initial state
canvas = renderer(domain, state)
save("gridModif.png",canvas)


plan = @pddl("(down)", "(down)")
renderer(canvas, domain, state, plan)
trajectory = PDDL.simulate(domain, state, plan)
canvas = renderer(domain, trajectory)
#=
# Render plan
#plan = @pddl("(right)", "(right)", "(right)", "(up)", "(up)")

renderer(canvas, domain, state, plan)
# Render trajectory
trajectory = PDDL.simulate(domain, state, plan)

canvas = renderer(domain, trajectory)


#=
# Render path search solution
astar = AStarPlanner(GoalCountHeuristic(), save_search=true,
                     save_search_order=true, max_nodes=100)
sol = astar(domain, state, pddl"(has gem1)")
canvas = renderer(domain, state, sol)
save("gridSol.png",canvas)



# Render policy solution
heuristic = PlannerHeuristic(AStarPlanner(GoalCountHeuristic(), max_nodes=20))
rtdp = RTDP(heuristic=heuristic, n_rollouts=5, max_depth=20)
policy = rtdp(domain, state, pddl"(has gem1)")
canvas = renderer(domain, state, policy)


# Animate plan
plan = collect(sol)
anim = anim_plan(renderer, domain, state, plan; trail_length=10)
save("doors_keys_gems.mp4", anim)

# Animate path search planning
canvas = renderer(domain, state)
sol_anim, sol = anim_solve!(canvas, renderer, astar,
                            domain, state, pddl"(has gem1)")
save("doors_keys_gems_astar.mp4", sol_anim)


# Animate RTDP planning
canvas = renderer(domain, state)
sol_anim, sol = anim_solve!(canvas, renderer, rtdp,
                            domain, state, pddl"(has gem2)")
#save("doors_keys_gems_rtdp_NoSol.mp4", sol_anim)
save("doors_keys_gems_rtdp_NoSol.png", canvas)

# Animate RTDP planning
canvas = renderer(domain, state)
sol_anim, sol = anim_solve!(canvas, renderer, rtdp,
                            domain, state, pddl"(has gem1)")
save("doors_keys_gems_rtdp.mp4", sol_anim)
save("doors_keys_gems_rtdp_Sol.png", canvas)


# Animate RTHS planning
rths = RTHS(GoalCountHeuristic(), n_iters=5, max_nodes=15)
canvas = renderer(domain, state)
sol_anim, sol = anim_solve!(canvas, renderer, rths,
                            domain, state, pddl"(has gem1)")
save("doors_keys_gems_rths.mp4", sol_anim)

=#

# Convert animation frames to storyboard
storyboard = render_storyboard(
    anim, [1, 14, 17, 24], figscale=0.75,
    xlabels=["t=1", "t=14", "t=17", "t=24"],
    subtitles=["(i) Initial state", "(ii) Agent picks up key",
               "(iii) Agent unlocks door", "(iv) Agent picks up gem"],
    xlabelsize=18, subtitlesize=22
)

=#
# Construct multiple canvases on the same figure
figure = Figure(resolution=(1200, 700))
canvas1 = new_canvas(renderer, figure[1, 1])
canvas2 = new_canvas(renderer, figure[1, 2])
renderer(canvas1, domain, state)
renderer(canvas2, domain, state, plan)

# Add controller
canvas = renderer(domain, state)
controller = KeyboardController(
    Keyboard.up => pddl"(up)",
    Keyboard.down => pddl"(down)",
    Keyboard.left => pddl"(left)",
    Keyboard.right => pddl"(right)",
    Keyboard.z, Keyboard.x, Keyboard.c, Keyboard.v
)
add_controller!(canvas, controller, domain, state; show_controls=true)


#remove_controller!(canvas, controller)

canvas = renderer(domain, state)
state = execute(domain, state, pddl"down")
canvas = renderer(domain, state)
state = execute(domain, state, pddl"down")
canvas = renderer(domain, state)
state = execute(domain, state, pddl"up")
canvas = renderer(domain, state)

state = execute(domain, state, pddl"pickup")
canvas = renderer(domain, state)

#iterative mode

#=
state = execute(domain, state, pddl"right")
state = execute(domain, state, pddl"right")
state = execute(domain, state, pddl"right")
state = execute(domain, state, pddl"right")



using DataStructures


stateI = initstate(domain, problem)
state = stateI

# create a new Queue
queuePlan = Queue{Compound}()
for a in plan
enqueue!(queuePlan, a)
end

canvas = renderer(domain, state)

i=0
function iterateOnPlan(canvas,domain,state)
    action = dequeue!(queuePlan)
    println(action)
#    state = execute(domain, state, pddl"up")
    state = execute(domain, state, action)
    #canvas = renderer(domain, state) #not working inside function
    return state
end


#state = iterateOnPlan(canvas,domain,state)
#canvas = renderer(domain, state)

for a in plan
    state = iterateOnPlan(canvas,domain,state)
    canvas = renderer(domain, state)
    save("gridExec_"*string(i)*".png",canvas)
    i = i+1
end

#TODO: 
#state = execute(domain, state, pddl"toto")
#ERROR: KeyError: key :toto not found

=#



