# Overcooked Visu

## Build

### Dependecies

Overcooked needs the SDL2 and the extensions SDL2_ttf and SDL2_image development files to build. 

On Ubuntu type `sudo apt install libsdl2-dev libsdl2-ttf-dev libsdl2-image-dev`

At SI, on the RedHat machines, the dependencies are already installed

### Build

Run `make` in this folder to build the software

## Run

Place your terminal in the OvercookedVisu folder and run `./overcooked <path/to/solution/file>` to launch the simulation.

If the path to your solution file has not changed, you can click on the refresh button to load a new solution and visualize it.
