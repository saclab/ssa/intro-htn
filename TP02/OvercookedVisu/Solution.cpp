#include "Solution.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>


Solution::Solution(char* filename)
{
	_file_location = filename;
	_states.push_back((State_t){.pos=_coordinatesOfLocation(Faraway)});
	_current_step = 0;
}

bool Solution::load(){
	_actions.clear();
	_actions_strings.clear();
	_states.clear();

	std::ifstream file(_file_location); 
	if (!file.is_open()) {  // Check if the file was successfully opened
		std::cerr << "Error: Could not open the file!" << _file_location<< std::endl;
		exit(-1);
	}

	State_t currentState = {
		.pos=_coordinatesOfLocation(Faraway),
		.holdsOnion=0,
		.holdsChoppedOnion =0,
		.holdsPlate = false,
		.onionOnChoppingBoard=0,
		.choppedOnionOnChoppingBoard=0,
		.nbOnionsInCook=0,
		.soupReady = false,
		.score=0,
		.steps=0};
	_states.push_back(currentState);

	bool solution_started = false;
	int line_nb = 0;
	std::string line;
	while (std::getline(file, line)) {
		line_nb++;
		if (line.find("-----------------------Plan-----------------------") == 0){
			solution_started = true;
			continue;
		}
		if (!solution_started) continue;
		if (line.find("Total") == 0) break;//Fin du plan

		std::string err;

		Action_t action = _actionFromString(line, err, line_nb);
		_actions.push_back(action);

		if (action.action == Move ||
		    action.action == Pickup ||
		    action.action == Drop ||
		    action.action == Chop ||
		    action.action == Cook ||
		    action.action == Serve)
		{
			currentState.steps++;
		}

		//Check de la position
		if (action.action == Pickup ||
		    action.action == Drop ||
		    action.action == Chop ||
		    action.action == Cook ||
		    action.action == Serve)
		{
			if (currentState.pos.x == _coordinatesOfLocation(Faraway).x){
				for (size_t i = 0; i < _states.size() ; i++){
					_states[i].pos = _coordinatesOfLocation(action.where);
					currentState.pos = _coordinatesOfLocation(action.where);
				}
			}
		}

		if (action.action == Move){
			currentState.pos = _coordinatesOfLocation(action.where);
		}
		if (action.action == Pickup){
			if (action.where == ChopLoc){
				if (action.what == Onion){
					currentState.holdsOnion++;
					currentState.onionOnChoppingBoard--;
				} else if (action.what == OnionChopped1){
					currentState.holdsChoppedOnion+=1;
					currentState.choppedOnionOnChoppingBoard-=1;
				} else if (action.what == OnionChopped2){
					currentState.holdsChoppedOnion+=2;
					currentState.choppedOnionOnChoppingBoard-=2;
				} else if (action.what == OnionChopped3){
					currentState.holdsChoppedOnion+=3;
					currentState.choppedOnionOnChoppingBoard-=3;
				}
			} else if (action.where == CookLoc){
				if (action.what == Onion){
				} else if (action.what == OnionChopped1){
					currentState.holdsChoppedOnion+=1;
					currentState.choppedOnionOnChoppingBoard-=1;
				} else if (action.what == OnionChopped2){
					currentState.holdsChoppedOnion+=2;
					currentState.choppedOnionOnChoppingBoard-=2;
				} else if (action.what == OnionChopped3){
					currentState.holdsChoppedOnion+=3;
					currentState.choppedOnionOnChoppingBoard-=3;
				} else {
					currentState.holdsPlate = true;
					//On détecte qu’on était sur un problème avec une soupe déjà prête, on rétro-propage
					if (!currentState.soupReady){
						for (size_t i = 0; i < _states.size() ; i++){
							_states[i].soupReady = true;
						}
					}
					currentState.soupReady = false;
				}
			} else {
				if (action.what == Onion){
					currentState.holdsOnion++;
				} else if (action.what == OnionChopped1){
					currentState.holdsChoppedOnion++;
				} else if (action.what == OnionChopped2){
					currentState.holdsChoppedOnion+=2;
				} else if (action.what == OnionChopped3){
					currentState.holdsChoppedOnion+=3;
				}
			}
		}
		if (action.action == Drop){
			if (action.where == ChopLoc){
				if (action.what == Onion){
					currentState.holdsOnion--;
					currentState.onionOnChoppingBoard++;
				} else if (action.what == OnionChopped1){
					currentState.holdsChoppedOnion--;
					currentState.choppedOnionOnChoppingBoard++;
				} else if (action.what == OnionChopped2){
					currentState.holdsChoppedOnion-=2;
					currentState.choppedOnionOnChoppingBoard+=2;
				} else if (action.what == OnionChopped3){
					currentState.holdsChoppedOnion-=3;
					currentState.choppedOnionOnChoppingBoard+=3;
				}
			} else if (action.where == CookLoc){
				if (action.what == Onion){
					currentState.holdsOnion--;
				} else if (action.what == OnionChopped1){
					currentState.holdsChoppedOnion -= 1;
					currentState.nbOnionsInCook += 1;
				} else if (action.what == OnionChopped2){
					currentState.holdsChoppedOnion -= 2;
					currentState.nbOnionsInCook += 2;
				} else if (action.what == OnionChopped3){
					currentState.holdsChoppedOnion -= 3;
					currentState.nbOnionsInCook += 3;
				}
			}
		}
		if (action.action == Cook){
			if (action.what == Soup){
				if (currentState.nbOnionsInCook < 3){
					for (size_t i = 0; i < _states.size() ; i++){
						_states[i].nbOnionsInCook += (3-currentState.nbOnionsInCook);
					}
				}
			}
			if (action.what == SoupLite){
				if (currentState.nbOnionsInCook < 1){
					for (size_t i = 0; i < _states.size() ; i++){
						_states[i].nbOnionsInCook += (1-currentState.nbOnionsInCook);
					}
				}
			}
			currentState.nbOnionsInCook = 0;
			currentState.soupReady = true;
		}
		if (action.action == Serve){
			currentState.holdsOnion = false;
			currentState.holdsChoppedOnion = false;
			currentState.holdsPlate = false;
			currentState.score++;
		}
		if (action.action == Chop){
			switch (action.what){
				case OnionChopped1:
					currentState.choppedOnionOnChoppingBoard++;
					currentState.onionOnChoppingBoard--;
					break;
				case OnionChopped2:
					currentState.choppedOnionOnChoppingBoard+=2;
					currentState.onionOnChoppingBoard-=2;
					break;
				case OnionChopped3:
					currentState.choppedOnionOnChoppingBoard+=3;
					currentState.onionOnChoppingBoard-=3;
					break;
				default:;
			}
			if (currentState.onionOnChoppingBoard<0){
				for (size_t i = 0; i < _states.size() ; i++){
					_states[i].onionOnChoppingBoard += -currentState.onionOnChoppingBoard;
				}
				currentState.onionOnChoppingBoard=0;

			}
		}
		_states.push_back(currentState);
		_actions_strings.push_back(line);

	}

	if (!solution_started){
		std::cerr << "Error, no solution in file:" << _file_location;
		exit(-1);
	}

	_current_step = 0;

	//_actions_strings = getActions();

	return true;
}


void Solution::setCurrentStateIndex(int index){
	if (index != _current_step){
		_current_step = index;
	}
}

Solution::ActionType_t Solution::_actionTypeFromString(std::string str, std::string &err){
	if (str == "move")         return ActionType_t::Move;
	if (str.find("pickup")==0) return ActionType_t::Pickup;
	if (str.find("drop")==0)   return ActionType_t::Drop;
	if (str == "chop")         return ActionType_t::Chop;
	if (str == "cook")         return ActionType_t::Cook;
	if (str == "serve")        return ActionType_t::Serve;

	err = "Invalid action name: " + str;
	//qDebug()<<err;
	return ActionType_t::Noop;
}

Solution::Agent_t Solution::_agentFromString(std::string str, std::string &err){
	if (str == "a1") return Agent_t::A1;

	err = "Invalid agent name: " + str;
	//qDebug()<<err;
	return Agent_t::A1;
}

Solution::Object_t Solution::_objectFromString(std::string str, std::string &err){
	if (str.find("onion1chopped") == 0) return Object_t::OnionChopped1;
	if (str.find("onion2chopped") == 0) return Object_t::OnionChopped2;
	if (str.find("onion3chopped") == 0) return Object_t::OnionChopped3;
	if (str.find("onion") == 0)         return Object_t::Onion;
	if (str.find("souplite") == 0)      return Object_t::SoupLite;

	if (str.find("soup") == 0)          return Object_t::Soup;

	err = "Invalid object name: " + str;
	//qDebug()<<err;
	return Object_t::Onion;
}
Solution::Location_t Solution::_locationFromString(std::string str, std::string &err){
	if (str == "faraway")     return Location_t::Faraway;
	if (str == "centerloc")   return Location_t::CenterLoc;
	if (str == "stock")       return Location_t::StockLoc;
	if (str == "choploc")     return Location_t::ChopLoc;
	if (str == "cookloc")     return Location_t::CookLoc;
	if (str == "deliveryloc") return Location_t::DeliveryLoc;

	err = "Invalid place name: " + str;
	//qDebug()<<err;
	return Location_t::Faraway;
}


Solution::Action_t Solution::_actionFromString(std::string str, std::string &err, int line_number){
	Action_t action;
	err = "";

	std::replace(str.begin(), str.end(), '(', ' ');
	std::replace(str.begin(), str.end(), ')', ' ');
	std::vector<std::string> tokens;
	// Create a string stream from the input string
	std::istringstream iss(str);
	std::string word;
	
	// Extract words separated by spaces and store them in the vector
	while (iss >> word) {
		tokens.push_back(word);
	}

	if (tokens.size() < 2){
		std::cerr<<"Error line "<<line_number<<": no action"<<std::endl;
		exit(-1);
	}

	ActionType_t action_type = _actionTypeFromString(tokens[1], err);

	if (err != ""){
		return action;//empty action
	}

	switch (action_type){
	case Move:
	{
		Agent_t agent = _agentFromString(tokens[2], err);
		Location_t start = _locationFromString(tokens[3], err);
		Location_t loc = _locationFromString(tokens[4], err);
		return (Action_t){.action = ActionType_t::Move, .who = agent, .start = start, .where = loc};
		break;
	}
	case Pickup:
	{
		Agent_t agent = _agentFromString(tokens[2], err);
		Object_t object = _objectFromString(tokens[tokens.size()-2], err);
		Location_t loc = _locationFromString(tokens[tokens.size()-1], err);
		return (Action_t){.action = ActionType_t::Pickup, .who = agent, .what = object, .where = loc};
		break;
	}
	case Drop:
	{
		Agent_t agent = _agentFromString(tokens[2], err);
		Object_t object = _objectFromString(tokens[tokens.size()-2], err);
		Location_t loc = _locationFromString(tokens[tokens.size()-1], err);
		return (Action_t){.action = ActionType_t::Drop, .who = agent, .what = object, .where = loc};
		break;
	}
	case Chop:
	{
		Agent_t agent = _agentFromString(tokens[2], err);
		Object_t object = _objectFromString(tokens[4], err);
		Location_t loc = _locationFromString(tokens[5], err);
		return (Action_t){.action = ActionType_t::Chop, .who = agent, .what = object, .where = loc};
		break;
	}
	case Cook:
	{
		Agent_t agent = _agentFromString(tokens[2], err);
		Object_t object2 = _objectFromString(tokens[4], err);
		Location_t loc = _locationFromString(tokens[5], err);
		return (Action_t){.action = ActionType_t::Cook, .who = agent, .what = object2, .where = loc};
		break;
	}
	case Serve:
	{
		Agent_t agent = _agentFromString(tokens[2], err);
		Object_t object = _objectFromString(tokens[3], err);
		Location_t loc = _locationFromString(tokens[4], err);
		return (Action_t){.action = ActionType_t::Serve, .who = agent, .what = object, .where = loc};
		break;
	}
	case Noop:
	{
		return (Action_t){.action = ActionType_t::Noop};
		break;
	}
	}

	return action;
}


Solution::Point_t Solution::_coordinatesOfLocation(Location_t loc){
	switch (loc){
	case Faraway:
		return (Point_t){.x=-10,.y=-10};
	case CenterLoc:
		return (Point_t){.x=5,.y=3};
	case StockLoc:
		return (Point_t){.x=10,.y=3};
	case ChopLoc:
		return (Point_t){.x=7,.y=6};
	case CookLoc:
		return (Point_t){.x=5,.y=6};
	case DeliveryLoc:
		return (Point_t){.x=5,.y=0};
	}
	return (Point_t){.x=-10,.y=-10};
}
