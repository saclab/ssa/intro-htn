# TP01 - Practice - Introduction to HTN planning

- [TP01 - Practice - Introduction to HTN planning](#tp01---practice---introduction-to-htn-planning)
- [First HDDL](#first-hddl)
  - [Resources](#resources)
  - [Set-up project environment](#set-up-project-environment)
    - [On SI computers](#on-si-computers)
  - [Testing Set-up](#testing-set-up)
  - [Adding shortcuts](#adding-shortcuts)
    - [Option 1: sourcing env](#option-1-sourcing-env)
    - [Option 2: manually](#option-2-manually)
  - [First plan](#first-plan)
  - [Writing some tasks, methods and literals](#writing-some-tasks-methods-and-literals)
  - [Adding cost](#adding-cost)
    - [Some help if necessary](#some-help-if-necessary)
      - [in the domain](#in-the-domain)
      - [in the problem](#in-the-problem)
  - [Adding cash machines](#adding-cash-machines)
- [Travelling salesman problem](#travelling-salesman-problem)
  - [Writing a more interesting planning problem](#writing-a-more-interesting-planning-problem)
  - [Some statistics and performance](#some-statistics-and-performance)


# First HDDL 

## Resources

-  [PDDL and HDDL quick-start tutorial](externalRessources/tutorialPDDL_HDDL.pdf)
-  [HyperTensioN webpage](https://github.com/Maumagnaguagno/HyperTensioN)
-  [Other resources](https://gitlab.isae-supaero.fr/saclab/ssa/intro-htn/-/tree/main/externalRessources)

## Set-up project environment

- [ ] Clone git repository

```
mkdir -p git && cd git
git clone https://gitlab.isae-supaero.fr/saclab/ssa/intro-htn.git
cd intro-htn
HTN_PATH=$(pwd)
```


### On SI computers

- [ ] load SI environment

```
cd $HTN_PATH
module load julia/1.9.3 
module load anaconda3/2023
```


## Testing Set-up 

- [ ] Test HyperTensioN planner

```
cd $HTN_PATH
cd HyperTensioN
ruby Hype.rb examples/basic/basic.jshop examples/basic/pb1.jshop rb
ruby examples/basic/pb1.jshop.rb

# should print: 
#----------------------Tasks-----------------------
#0: swap(banjo kiwi)
#---------------------Planning---------------------
#Time: 4.076957702636719e-05s
#-----------------------Plan-----------------------
#0: drop(kiwi)
#1: pickup(banjo)
```

## Adding shortcuts 

- [ ] add some macros for easy execution



### Option 1: sourcing env

```
cd $HTN_PATH
source loadEnv.bash
```


### Option 2: manually

```
#HyperTensioN execution command: 
function plan {
ruby ${HTN_PATH}/HyperTensioN/Hype.rb $1 $2 $3
}
alias plan='plan'

#Ruby dot:
function planviz {
plan $1 $2 dot
dot -Tpdf "$1".dot > "$1".pdf && 
evince "$1".pdf &
}
alias planviz='planviz'
```

- [ ] test it with your future HDDL domain

```
cd ${HTN_PATH}/TP01
planviz travelWalkTaxi.hddl gotoPark_problem.hddl 
```

## First plan

Suppose that you want a planner to help you to decide how you will achieve some travelling tasks in a foreign city.

Have a look to the __domain HDDL file__ <travelWalkTaxi.hddl> and the __problem HDDL file__  <gotoPark_problem.hddl> with your favorite editor

- [ ] Check that you can get a valid plan (only one action is performed actually!)

In this dummy example, we want to travel from home to the park, corresponding to the goal __Task__ in <gotoPark_problem.hddl>: *(travel home park)*

```
plan travelWalkTaxi.hddl gotoPark_problem.hddl debug
```
You should get a dummy plan.

- [ ] Check what's happening if the park is not at a short distance from home...

> i.e remove '(shortDistance home park)' predicate.


## Writing some tasks, methods and literals

- [ ] In the problem file, add an *airport* location, and try to travel there 

Note that the airport is not at a $shortDistance$ from home or from the park.

>You may create a new __problem HDDL file__ <gotoAirport_problem.hddl>


Now, before taking your flight, you would like to go to the park, and then reach the airport. Update the task list in the problem (or the compound task and methods in the domain?) according to.


- [ ] Generate a plan where you're going first to the park, and after to the airport.

> You may create a new __problem HDDL file__ <gotoAirportFromPark_problem.hddl>




## Adding cost

Actually, taxi ride is not free. To keep it simple, each travel in taxi will cost you $20, no matter the distance. Begin with $60 cash in your pocket. In this city, taxis only accept cash.

- [ ] Model the taxi cost: in the domain, add an action 'pay_driver'

&nbsp;

> You may create a new __domain HDDL file__ <travelWalkTaxiCost.hddl> and __problem HDDL file__ <travelWalkTaxiCost_problem.hddl>

&nbsp;

> Trick:  use a "gauge" for the amount of money, i.e. objects that represent your value (numerable are not implemented yet and would complexify the problem resolution!)

&nbsp;

> Warning: 'functions' are not implemented in HyperTensioN



![OpenAItaxi.png](OpenAItaxi.png "OpenAI Gym tutorial")

Figure 1: [OpenAI Gym tutorial](https://www.gocoder.one/blog/rl-tutorial-with-openai-gym/)

&nbsp;

### Some help if necessary

#### in the domain

in a method or task, you will need an artificial precondition to pass from $60 to $40: 

```
 (next ?l2 ?l1)
```


```
 	(:action pay_driver 
     [...]

 	:effect (and 
				   (not (money-level ?l2))
				   (money-level ?l1)
	        )
    )
```

#### in the problem

```
(:objects 
    ...
    $0 $20 $40 $60 - Cash
)

(:init 
        ...
		(next $60 $40)
		(next $40 $20)
		(next $20 $0)						
		(money-level $20)

)

```
## Adding cash machines

- [ ] Add more destinations and possibilities

For example, add a museum that you actually want to visit since you have time before your flight.

> At each step, for the moment, chack that you have enough cash and that your still getting a valid plan.


Now, try it again, with only $20 in your wallet: planning should fail if you are visiting a museum far away...

- [ ] Add a 'CashMachine' locations and generate a new plan beginning without enough cash.

Cash Machines are located at short distance from home, park and museum (apparently we are in a capitalist city). From there, you can retrieve cash. Don't worry, we will not model your bank account.

> You may create a new __domain HDDL file__ <travelManyPlaces.hddl> and __problem HDDL file__ <travelManyPlaces_problem.hddl>


# Travelling salesman problem

## Writing a more interesting planning problem

- [ ] Add 10 destinations in the city and visit them all before getting to the airport.

 E.g add a library, coffee-shop...  Some are at short-distance from one another, others not.  

> check that you still getting a valid plan.
> you may still have to modify your porblem and domain, e.g adding a predicate 'hasBeenVisited' for a location


## Some statistics and performance

- [ ] evaluate the performance of the planner 

 >- how many steps for 10 locations?
 >- how much CPU time for 10 locations?
 >- draw a plot (e.g in Matplotlib) for time and cost according to problem size (e.g: number of locations here)



![PDDL.png](pddl.png "PDDL")

