#include "Display.h"
#include <SDL2/SDL_image.h>
#include <iostream>

const char* chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&/*()-_+=: ";

Display::Display(Solution *p_solution, std::string pathToExe) : window(nullptr), renderer(nullptr), background(nullptr) {
	_sol = p_solution;
	_pathToExe = pathToExe;
}

bool Display::init(const std::string& title) {
	int width = backgroundWidth+actionsColWidth;
	int height = backgroundHeight+buttonsSize;
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		std::cerr << "Failed to initialize SDL: " << SDL_GetError() << std::endl;
		return false;
	}

	window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN);
	if (!window) {
		std::cerr << "Failed to create window: " << SDL_GetError() << std::endl;
		return false;
	}
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (!renderer) {
		std::cerr << "Failed to create renderer: " << SDL_GetError() << std::endl;
		return false;
	}

	// Load the background texture
	background = loadTexture(_pathToExe+"assets/background.png");
	if (!background) {
		std::cerr << "Failed to load background texture" << std::endl;
		return false;
	}
	perso = loadTexture(_pathToExe+"assets/Perso.png");
	if (!perso) {
		std::cerr << "Failed to load perso texture" << std::endl;
		return false;
	}
	play = loadTexture(_pathToExe+"assets/Play.png");
	if (!perso) {
		std::cerr << "Failed to load play texture" << std::endl;
		return false;
	}
	pause = loadTexture(_pathToExe+"assets/Pause.png");
	if (!perso) {
		std::cerr << "Failed to load pause texture" << std::endl;
		return false;
	}
	reload = loadTexture(_pathToExe+"assets/Reload.png");
	if (!reload) {
		std::cerr << "Failed to load reload texture" << std::endl;
		return false;
	}
	onion = loadTexture(_pathToExe+"assets/Onion.png");
	if (!onion) {
		std::cerr << "Failed to load Onion texture" << std::endl;
		return false;
	}
	choppedOnion = loadTexture(_pathToExe+"assets/OnionChopped.png");
	if (!choppedOnion) {
		std::cerr << "Failed to load OnionChopped texture" << std::endl;
		return false;
	}
	plate = loadTexture(_pathToExe+"assets/Plate.png");
	if (!choppedOnion) {
		std::cerr << "Failed to load Plate texture" << std::endl;
		return false;
	}

	slider = { buttonsSize*3, backgroundHeight, backgroundWidth - (buttonsSize*3 + nbWidth*2), buttonsSize, 20, 0, false }; // x, y, width, height, handleWidth, initial value

	font24Map = loadTexture(_pathToExe+"assets/Courier_New24.bmp");
	if (!font24Map) {
		std::cerr << "Failed to load font texture" << std::endl;
		return false;
	}
	SDL_QueryTexture(font24Map, NULL, NULL, &font24Width, &font24Height);
	font24Width /= strlen(chars);

	font18Map = loadTexture(_pathToExe+"assets/Courier_New18.bmp");
	if (!font18Map) {
		std::cerr << "Failed to load font texture" << std::endl;
		return false;
	}
	SDL_QueryTexture(font18Map, NULL, NULL, &font18Width, &font18Height);
	font18Width /= strlen(chars);

	return true;
}

SDL_Texture* Display::loadTexture(const std::string& path) {
	SDL_Texture* texture = IMG_LoadTexture(renderer, path.c_str());
	if (!texture) {
		std::cerr << "Failed to load texture: " << IMG_GetError() << std::endl;
	}
	return texture;
}

void Display::render() {
	if (playing){
		if ((clock() - lastFrame) > CLOCKS_PER_SEC*0.3){
			_sol->setCurrentStateIndex(_sol->getCurrentStateIndex()+1);
			if (_sol->getCurrentStateIndex() >= _sol->getNbStates()){
				playing = false;
			}
			lastFrame = clock();
		}
	}

	SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255); // default color

	SDL_RenderClear(renderer);

	// Render background
	SDL_Rect backgroundDest = {.x=0, .y=0, .w=backgroundWidth, .h=backgroundHeight};
	SDL_RenderCopy(renderer, background, nullptr, &backgroundDest);

	//Render Buttons
	SDL_Rect reloadDest = {.x=0, .y=backgroundHeight, .w=buttonsSize, .h=buttonsSize};
	SDL_RenderCopy(renderer, reload, nullptr, &reloadDest);
	SDL_Rect playDest = {.x=buttonsSize, .y=backgroundHeight, .w=buttonsSize, .h=buttonsSize};
	SDL_RenderCopy(renderer, play, nullptr, &playDest);
	SDL_Rect pauseDest = {.x=buttonsSize+buttonsSize, .y=backgroundHeight, .w=buttonsSize, .h=buttonsSize};
	SDL_RenderCopy(renderer, pause, nullptr, &pauseDest);
	
	//Render Slider and progress index
	renderSlider();
	SDL_Rect frameNbRect = {.x=backgroundWidth-(nbWidth*2), .y=backgroundHeight, .w=buttonsSize, .h=buttonsSize};
	renderText(" "+std::to_string(_sol->getCurrentStateIndex()), frameNbRect, 24);
	SDL_Rect totFrameNbRect = {.x=backgroundWidth-nbWidth, .y=backgroundHeight, .w=buttonsSize, .h=buttonsSize};
	renderText("/" + std::to_string(_sol->getNbStates()), totFrameNbRect, 24);

	//Render game elements
	renderPerso();
	renderChoppingPlate();
	renderCookingPot();

	renderActions();
	renderScore();

	SDL_RenderPresent(renderer);
}

void Display::renderPerso(){
	Solution::State_t currentState = _sol->getCurrentState();
	Solution::Point_t gridPos = currentState.pos;
	int fullImgPosY = ((gridPos.y)/(gridHeight-1.) * (bottomLefty - topLefty)) + topLefty;
	int topx    = ((gridPos.x)/(gridWidth-1.) * (topRightx - topLeftx)) + topLeftx;
	int bottomx = ((gridPos.x)/(gridWidth-1.) * (bottomRightx - bottomLeftx)) + bottomLeftx;
	int fullImgPosX = topx * (1-((gridPos.y)/(gridWidth-1.))) + bottomx * (gridPos.y)/(gridWidth-1.);

	SDL_Rect persoDest = {.x=fullImgPosX - (persoWidth/2), .y=fullImgPosY - (persoHeight/2), .w=persoWidth, .h=persoHeight};
	SDL_RenderCopy(renderer, perso, nullptr, &persoDest);

	if (currentState.holdsOnion){
		SDL_Rect onionDest = {.x=fullImgPosX - (50/2), .y=fullImgPosY, .w=50, .h=50};
		SDL_RenderCopy(renderer, onion, nullptr, &onionDest);
	}
	if (currentState.holdsChoppedOnion){
		//SDL_Rect choppedOnionDest = {.x=fullImgPosX - (50/2), .y=fullImgPosY, .w=50, .h=50};
		//SDL_RenderCopy(renderer, choppedOnion, nullptr, &choppedOnionDest);
		SDL_Rect onionDest;
		switch (currentState.holdsChoppedOnion){
		case 3:
			onionDest = {.x=fullImgPosX-10, .y=fullImgPosY+10, .w=30, .h=30};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &onionDest);
			[[fallthrough]];
		case 2:
			onionDest = {.x=fullImgPosX-20, .y=fullImgPosY+30, .w=30, .h=30};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &onionDest);
			[[fallthrough]];
		case 1:
			onionDest = {.x=fullImgPosX, .y=fullImgPosY+30, .w=30, .h=30};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &onionDest);
		}
	}
	if (currentState.holdsPlate){
		SDL_Rect plateDest = {.x=fullImgPosX - (50/2), .y=fullImgPosY, .w=50, .h=50};
		SDL_RenderCopy(renderer, plate, nullptr, &plateDest);
	}
}

void Display::renderChoppingPlate(){
	Solution::State_t currentState = _sol->getCurrentState();
	Solution::Point_t gridPos = {.x=7, .y=7};
	int fullImgPosY = ((gridPos.y)/(gridHeight-1.) * (bottomLefty - topLefty)) + topLefty;
	int topx    = ((gridPos.x)/(gridWidth-1.) * (topRightx - topLeftx)) + topLeftx;
	int bottomx = ((gridPos.x)/(gridWidth-1.) * (bottomRightx - bottomLeftx)) + bottomLeftx;
	int fullImgPosX = topx * (1-((gridPos.y)/(gridWidth-1.))) + bottomx * (gridPos.y)/(gridWidth-1.);


	if (currentState.onionOnChoppingBoard){
		SDL_Rect onionDest;
		switch (currentState.onionOnChoppingBoard){
		case 6:
			onionDest = {.x=fullImgPosX+10, .y=fullImgPosY+30, .w=30, .h=30};
			SDL_RenderCopy(renderer, onion, nullptr, &onionDest);
			[[fallthrough]];
		case 5:
			onionDest = {.x=fullImgPosX, .y=fullImgPosY+50, .w=30, .h=30};
			SDL_RenderCopy(renderer, onion, nullptr, &onionDest);
			[[fallthrough]];
		case 4:
			onionDest = {.x=fullImgPosX+20, .y=fullImgPosY+50, .w=30, .h=30};
			SDL_RenderCopy(renderer, onion, nullptr, &onionDest);
			[[fallthrough]];
		case 3:
			onionDest = {.x=fullImgPosX+0, .y=fullImgPosY+10, .w=30, .h=30};
			SDL_RenderCopy(renderer, onion, nullptr, &onionDest);
			[[fallthrough]];
		case 2:
			onionDest = {.x=fullImgPosX+20, .y=fullImgPosY+10, .w=30, .h=30};
			SDL_RenderCopy(renderer, onion, nullptr, &onionDest);
			[[fallthrough]];
		case 1:
			onionDest = {.x=fullImgPosX+30, .y=fullImgPosY+30, .w=30, .h=30};
			SDL_RenderCopy(renderer, onion, nullptr, &onionDest);
		}
	}
	if (currentState.choppedOnionOnChoppingBoard){
		SDL_Rect onionDest;
		switch (currentState.choppedOnionOnChoppingBoard){
		case 6:
			onionDest = {.x=fullImgPosX+0, .y=fullImgPosY+10, .w=30, .h=30};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &onionDest);
			[[fallthrough]];
		case 5:
			onionDest = {.x=fullImgPosX+20, .y=fullImgPosY+10, .w=30, .h=30};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &onionDest);
			[[fallthrough]];
		case 4:
			onionDest = {.x=fullImgPosX+30, .y=fullImgPosY+30, .w=30, .h=30};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &onionDest);
			[[fallthrough]];
		case 3:
			onionDest = {.x=fullImgPosX+10, .y=fullImgPosY+30, .w=30, .h=30};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &onionDest);
			[[fallthrough]];
		case 2:
			onionDest = {.x=fullImgPosX, .y=fullImgPosY+50, .w=30, .h=30};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &onionDest);
			[[fallthrough]];
		case 1:
			onionDest = {.x=fullImgPosX+20, .y=fullImgPosY+50, .w=30, .h=30};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &onionDest);
		}
	}
}

void Display::renderCookingPot(){
	Solution::State_t currentState = _sol->getCurrentState();

	Solution::Point_t gridPos = {.x=5, .y=7};
	int fullImgPosY = ((gridPos.y)/(gridHeight-1.) * (bottomLefty - topLefty)) + topLefty;
	int topx    = ((gridPos.x)/(gridWidth-1.) * (topRightx - topLeftx)) + topLeftx;
	int bottomx = ((gridPos.x)/(gridWidth-1.) * (bottomRightx - bottomLeftx)) + bottomLeftx;
	int fullImgPosX = topx * (1-((gridPos.y)/(gridWidth-1.))) + bottomx * (gridPos.y)/(gridWidth-1.);


	if (currentState.nbOnionsInCook){
		SDL_Rect choppedOnionDest;
		switch (currentState.nbOnionsInCook){
		case 3:
			choppedOnionDest = {.x=fullImgPosX-20, .y=fullImgPosY+45, .w=25, .h=25};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &choppedOnionDest);
			[[fallthrough]];
		case 2:
			choppedOnionDest = {.x=fullImgPosX-15, .y=fullImgPosY+65, .w=25, .h=25};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &choppedOnionDest);
			[[fallthrough]];
		case 1:
			choppedOnionDest = {.x=fullImgPosX-35, .y=fullImgPosY+65, .w=25, .h=25};
			SDL_RenderCopy(renderer, choppedOnion, nullptr, &choppedOnionDest);
		}
	}
	if (currentState.soupReady){
		SDL_Rect soupDest = {.x=fullImgPosX-31, .y=fullImgPosY+45, .w=52, .h=52};
		Uint8 r, g, b, a;
		SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);

		SDL_SetRenderDrawColor(renderer, 155, 99, 49, 255);
		drawEllipse(soupDest);
		SDL_SetRenderDrawColor(renderer, r, g, b, a); // default color
	}
}

void Display::drawEllipse(SDL_Rect ellipse)
{
	int radius = ellipse.w/2;
	int x = ellipse.x + radius;
	int y = ellipse.y + radius;

	int offsetx, offsety, d;
    int status;

    offsetx = 0;
    offsety = radius;
    d = radius -1;
    status = 0;

    while (offsety >= offsetx) {

        status += SDL_RenderDrawLine(renderer, x - offsety, y + offsetx,
                                     x + offsety, y + offsetx);
        status += SDL_RenderDrawLine(renderer, x - offsetx, y + offsety,
                                     x + offsetx, y + offsety);
        status += SDL_RenderDrawLine(renderer, x - offsetx, y - offsety,
                                     x + offsetx, y - offsety);
        status += SDL_RenderDrawLine(renderer, x - offsety, y - offsetx,
                                     x + offsety, y - offsetx);

        if (status < 0) {
            status = -1;
            break;
        }

        if (d >= 2*offsetx) {
            d -= 2*offsetx + 1;
            offsetx +=1;
        }
        else if (d < 2 * (radius - offsety)) {
            d += 2 * offsety - 1;
            offsety -= 1;
        }
        else {
            d += 2 * (offsety - offsetx - 1);
            offsety -= 1;
            offsetx += 1;
        }
    }
}

void Display::renderScore(){
	Solution::State_t currentState = _sol->getCurrentState();

	std::string score = std::to_string(currentState.score);
	std::string steps = std::to_string(currentState.steps);

	SDL_Rect scoreRect = {.x=scorex, .y=scorey, .w=200, .h=100};
	SDL_Rect scoreSteps = {.x=timex, .y=timey, .w=200, .h=100};

	renderText(score, scoreRect, 24);
	renderText(steps, scoreSteps, 24);
}

// Function to render the slider
void Display::renderSlider() {
	Uint8 r, g, b, a;
	SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);

	slider.value = ((float)_sol->getCurrentStateIndex()) / _sol->getNbStates();
	
	// Draw the slider track
	SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255); // Track color
	SDL_Rect track = { slider.x, slider.y + (slider.height / 2) - 5, slider.width, 10 };
	SDL_RenderFillRect(renderer, &track);

	// Calculate the position of the handle based on the value
	int handleX = slider.x + (slider.value * (slider.width - slider.handleWidth));

	// Draw the slider handle
	if (!slider.dragging){
		SDL_SetRenderDrawColor(renderer, 50, 50, 50, 255); // Handle color
	} else {
		SDL_SetRenderDrawColor(renderer, 150, 150, 150, 255); // Handle color
	}
	SDL_Rect handle = { handleX, slider.y, slider.handleWidth, slider.height };
	SDL_RenderFillRect(renderer, &handle);

	SDL_SetRenderDrawColor(renderer, r, g, b, a); // default color

}

// Function to render text in the text box
void Display::renderText(const std::string& text, SDL_Rect& rect, int size) {
	SDL_Texture* fontMap;
	int fontWidth;
	int fontHeight;
	if (size == 18){
		fontMap = font18Map;
		fontWidth = font18Width;
		fontHeight = font18Height;
	} else {
		fontMap = font24Map;
		fontWidth = font24Width;
		fontHeight = font24Height;
	}

	SDL_Rect cursor = {.x=rect.x, .y=rect.y, .w=fontWidth, .h=fontHeight};
	SDL_Rect character = {.x=0, .y=0, .w=fontWidth, .h=fontHeight};

	const char* ctext = text.c_str();
	
	for (size_t i = 0 ; i<strlen(ctext) ; i++){
		if (ctext[i] =='\n'){
			cursor.x = rect.x;
			cursor.y += fontHeight;
			continue;
		}
		character.x = ((int)(strchr(chars, ctext[i])-chars))*fontWidth;
		SDL_RenderCopy(renderer, fontMap, &character, &cursor);
		cursor.x += fontWidth;
		
		if ((ctext[i] == ' ') && (i != strlen(ctext-1))){
			char* nextSpace = strchr((char*)ctext+i+1, ' ');
			char* nextNewline = strchr((char*)ctext+i+1, '\n');
			if (nextNewline){
				if (nextSpace){
					if (nextNewline < nextSpace){
						nextSpace = nextNewline;
					}
				} else {
					nextSpace = nextNewline;
				}
			}
			int distToNextSpace = 0;
			if (nextSpace){
				distToNextSpace = nextSpace - (ctext+i);
			} else {
				distToNextSpace = strlen(ctext+i);
			}
			if (cursor.x + (distToNextSpace * fontWidth) > rect.x + rect.w){
				cursor.x = rect.x;
				cursor.y += fontHeight;
			}
		}
	}
	rect.h = cursor.y + fontHeight - rect.y;
}
void Display::measureText(const std::string& text, SDL_Rect& rect, int size) {
	int fontWidth;
	int fontHeight;
	if (size == 18){
		fontWidth = font18Width;
		fontHeight = font18Height;
	} else {
		fontWidth = font24Width;
		fontHeight = font24Height;
	}

	SDL_Rect cursor = {.x=rect.x, .y=rect.y, .w=fontWidth, .h=fontHeight};

	const char* ctext = text.c_str();

	for (size_t i = 0 ; i<strlen(ctext) ; i++){
		if (ctext[i] =='\n'){
			cursor.x = rect.x;
			cursor.y += fontHeight;
			continue;
		}
		cursor.x += fontWidth;
		
		if ((ctext[i] == ' ') && (i != strlen(ctext-1))){
			char* nextSpace = strchr((char*)ctext+i+1, ' ');
			char* nextNewline = strchr((char*)ctext+i+1, '\n');
			if (nextNewline){
				if (nextSpace){
					if (nextNewline < nextSpace){
						nextSpace = nextNewline;
					}
				} else {
					nextSpace = nextNewline;
				}
			}
			int distToNextSpace = 0;
			if (nextSpace){
				distToNextSpace = nextSpace - (ctext+i);
			} else {
				distToNextSpace = strlen(ctext+i);
			}
			if (cursor.x + (distToNextSpace * fontWidth) > rect.x + rect.w){
				cursor.x = rect.x;
				cursor.y += fontHeight;
			}
		}
	}
	rect.h = cursor.y + fontHeight - rect.y;
}


void Display::renderActions(){
	actionsEndHeight.clear();

	std::vector<std::string> actions = _sol->getActionsStrings();
	
	SDL_Rect currentCell = {.x=backgroundWidth, .y=-actionsScroll, .w=actionsColWidth, .h=500};
	for (size_t i = 0;i<actions.size();i++){
		measureText(actions[i], currentCell, 18);
		if ((int)i == _sol->getCurrentStateIndex()-1){
			SDL_SetRenderDrawColor(renderer, 0, 255, 255, 255);
		} else {
			SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
		}
		SDL_RenderFillRect(renderer, &currentCell);
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderDrawRect(renderer, &currentCell);
		renderText(actions[i], currentCell, 18);

		currentCell.y += currentCell.h;
		actionsEndHeight.push_back(currentCell.y);

	}
	actionsTotalHeight = currentCell.y + actionsScroll;
}

void Display::handleEvents(bool& running) {
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		if (event.type == SDL_QUIT) {
			running = false;
		}
		if (event.type == SDL_MOUSEBUTTONDOWN) {
			if (event.button.button == SDL_BUTTON_LEFT) {
				int mouseX = event.button.x;
				int mouseY = event.button.y;

				//Check if click is on the slider bar
				if (mouseX >= slider.x && mouseX <= slider.x + slider.width &&
					mouseY >= slider.y && mouseY <= slider.y + slider.height) 
				{
					slider.dragging = true;
					playing = false;
					slider.value = (mouseX - slider.x) * 1. / (slider.width);
					_sol->setCurrentStateIndex(round(_sol->getNbStates() * slider.value));
				}

				// Check if the mouse is on the Actions tab
				if (mouseX > backgroundWidth){
					//Find correct action
					for (int i = 0;i<_sol->getNbActions();i++){
						if (mouseY < actionsEndHeight[i]){
							_sol->setCurrentStateIndex(i+1);
							break;
						}
					}
				}

				// Check buttons
				if (mouseY > backgroundHeight){
					if (mouseX < buttonsSize){
						_sol->load();
						playing = false;
					} else if (mouseX < 2*buttonsSize){
						playing = true;
						lastFrame = clock();
						if (_sol->getCurrentStateIndex() >= _sol->getNbStates()){
							_sol->setCurrentStateIndex(0);
						}
					} else if (mouseX < 3*buttonsSize){
						playing = false;
					}
				}
			}
		}
		if (event.type == SDL_MOUSEBUTTONUP) {
			if (event.button.button == SDL_BUTTON_LEFT) {
				slider.dragging = false;
			}
		}
		if (event.type == SDL_MOUSEMOTION && slider.dragging) {
			int mouseX = event.motion.x;

			// Update the slider value based on mouse position
			if (mouseX < slider.x) {
				slider.value = 0;
			} else if (mouseX > slider.x + slider.width) {
				slider.value = 1;
			} else {
				slider.value = (mouseX - slider.x) * 1. / (slider.width);
			}
			_sol->setCurrentStateIndex(round(_sol->getNbStates() * slider.value));
		}
		if (event.type == SDL_MOUSEWHEEL ){
			actionsScroll -= event.wheel.y * 20;
			if (actionsScroll > actionsTotalHeight - (backgroundHeight+buttonsSize)){
				actionsScroll = (actionsTotalHeight - (backgroundHeight+buttonsSize));
			}
			if (actionsScroll < 0){
				actionsScroll = 0;
			}
		}
		// Handle other events like play/pause, progress bar, etc.
	}
}

void Display::cleanup() {
	SDL_DestroyTexture(background);
	SDL_DestroyTexture(perso);
	SDL_DestroyTexture(play);
	SDL_DestroyTexture(pause);
	SDL_DestroyTexture(reload);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}