#include <SDL.h>
#include <SDL_ttf.h>
#include <iostream>
#include <unordered_map>
#include <string>

// Bitmap font structure
struct BitmapFont {
	SDL_Surface* surface;	// The surface containing all characters
	std::unordered_map<char, SDL_Rect> charRects;  // Map of character positions in the atlas
	int fontHeight;  // Height of the font
};

// Function to create a bitmap font surface from a TTF font and save as BMP
BitmapFont createBitmapFont(TTF_Font* font, const std::string& characters, const std::string& bmpFilename) {
	BitmapFont bitmapFont;
	bitmapFont.fontHeight = TTF_FontHeight(font);

	int padding = 0;  // Padding between characters
	int textureWidth = 512; // Width of the texture atlas
	int textureHeight = bitmapFont.fontHeight + padding * 2; // Height will adjust

	// Calculate the width required for all characters
	int currentX = padding;
	int maxHeight = bitmapFont.fontHeight + padding * 2;
	int maxWidth = 0;
	int nbChars = 0;
	for (char c : characters) {
		int charWidth;
		TTF_GlyphMetrics(font, c, nullptr, nullptr, nullptr, nullptr, &charWidth);
		currentX += charWidth + padding;
		if (charWidth > maxWidth){
			maxWidth = charWidth;
		}
		nbChars++;
	}

	// Create an SDL surface to hold the bitmap font
	textureHeight = maxHeight;
	textureWidth = maxWidth*nbChars;
	SDL_Surface* surface = SDL_CreateRGBSurface(0, textureWidth, textureHeight, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
	if (!surface) {
		std::cerr << "Failed to create surface: " << SDL_GetError() << std::endl;
		exit(1);
	}

	// Now render each character to the surface
	currentX = 0;
	SDL_Color black = { 0, 0, 0, 255 };
	for (char c : characters) {
		SDL_Surface* glyphSurface = TTF_RenderGlyph_Blended(font, c, black);  // Render character in black
		if (!glyphSurface) {
			std::cerr << "Failed to render glyph: " << SDL_GetError() << std::endl;
			continue;
		}

		// Get the size of the glyph
		int charWidth = glyphSurface->w;
		int charHeight = glyphSurface->h;

		// Set character position in the surface
		SDL_Rect srcRect = { 0, 0, charWidth, charHeight };
		SDL_Rect destRect = { currentX, padding, charWidth, charHeight };
		SDL_BlitSurface(glyphSurface, &srcRect, surface, &destRect);

		// Store the rectangle for this character
		bitmapFont.charRects[c] = destRect;

		currentX += maxWidth;

		// Clean up the temporary surface
		SDL_FreeSurface(glyphSurface);
	}

	// Save the surface as a BMP file
	if (SDL_SaveBMP(surface, bmpFilename.c_str()) != 0) {
		std::cerr << "Failed to save BMP: " << SDL_GetError() << std::endl;
	} else {
		std::cout << "Bitmap font saved as " << bmpFilename << std::endl;
	}

	bitmapFont.surface = surface;

	return bitmapFont;
}

void generateSize(int size){
	// Load a font
	TTF_Font* font = TTF_OpenFont("assets/Courier_New.ttf", size);  // Adjust the font size and path
	if (!font) {
		std::cerr << "Failed to load font: " << TTF_GetError() << std::endl;
		TTF_Quit();
		SDL_Quit();
		exit(-1);
	}

	// Characters to include in the bitmap font
	std::string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&/*()-_+=: ";

	// Create the bitmap font and save it as a BMP
	std::string bmpFilename = "assets/Courier_New" + std::to_string(size) + ".bmp";
	BitmapFont bitmapFont = createBitmapFont(font, characters, bmpFilename);

	// Clean up
	SDL_FreeSurface(bitmapFont.surface);
	TTF_CloseFont(font);
}

int main() {
	// Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		std::cerr << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
		return -1;
	}

	// Initialize SDL_ttf
	if (TTF_Init() == -1) {
		std::cerr << "TTF_Init failed: " << TTF_GetError() << std::endl;
		SDL_Quit();
		return -1;
	}

	generateSize(24);
	generateSize(18);


	TTF_Quit();
	SDL_Quit();

	return 0;
}
