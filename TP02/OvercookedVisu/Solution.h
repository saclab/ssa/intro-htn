#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>
#include <vector>

class Solution
{

public:

	struct Point_t{
		int x;
		int y;
	};

	enum ActionType_t{
		Move,
		Pickup,
		Drop,
		Chop,
		Cook,
		Serve,
		Noop,
	};
	enum Agent_t{
		A1,
	};
	enum Object_t{
		Onion,
		OnionChopped1,
		OnionChopped2,
		OnionChopped3,
		Soup,
		SoupLite,
	};
	enum Location_t{
		Faraway,
		CenterLoc,
		StockLoc,
		ChopLoc,
		CookLoc,
		DeliveryLoc,
	};

	struct Action_t {
		ActionType_t action;
		Agent_t who;
		Object_t what;
		Location_t start;
		Location_t where;
	};

	struct State_t {
		Point_t pos;
		int holdsOnion;
		int holdsChoppedOnion;
		bool holdsPlate;
		int onionOnChoppingBoard;
		int choppedOnionOnChoppingBoard;
		int nbOnionsInCook;
		bool soupReady;
		int score;
		int steps;
	};

	bool getFileSet(){return _file_location != "";}
	int getNbActions(){return _actions.size();}
	std::vector<std::string> getActionsStrings(){return _actions_strings;}

	int getNbStates(){return _states.size() - 1;}
	int getCurrentStateIndex(){return _current_step;}
	State_t getCurrentState(){return _states[_current_step];}
	void setCurrentStateIndex(int index);
public:
	Solution(char* filename);

	bool load();

private:
	Point_t _coordinatesOfLocation(Location_t loc);

	ActionType_t _actionTypeFromString(std::string str, std::string &err);
	Agent_t _agentFromString(std::string str, std::string &err);
	Object_t _objectFromString(std::string str, std::string &err);
	Location_t _locationFromString(std::string str, std::string &err);
	Action_t _actionFromString(std::string str, std::string &err, int line_number);

private:
	std::string _file_location;
	std::vector<Action_t> _actions;
	std::vector<State_t> _states;
	std::vector<std::string> _actions_strings;

	int _current_step;

};

#endif // SOLUTION_H
