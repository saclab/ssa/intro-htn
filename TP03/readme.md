# Plan execution


## Resources

### Planimation

The [Planimation website](https://planimation.planning.domains/demo) and some exemples in "Grid/" folder

planimation02.png

### PDDLViz written in Julia


The [PDDLViz website](https://github.com/JuliaPlanners/PDDLViz.jl) and a running example in the "doors-keys-gems/" folder, with a 'example.jl' script.


### HyperTensioN

For solving your HDDL


## Goal: 

From doors-keys-gems, add human expertise! 
For example, get the key first...

### Present a valid HTN

### Optimize solving

### Call execution




