#ifndef DISPLAY_H
#define DISPLAY_H

#include "Solution.h"
#include <SDL2/SDL.h>
#include <string>

class Display {

	// Slider structure
struct Slider {
	int x, y;			// Position of the slider
	int width, height;   // Dimensions of the slider track
	int handleWidth;	 // Width of the slider handle
	float value;		   // Current value of the slider (0 to 1)
	bool dragging;	   // Is the slider handle being dragged?
};

public:
	Display(Solution *p_solution, std::string pathToExe);
	bool init(const std::string& title);
	void render();
	void handleEvents(bool& running);
	void cleanup();

private:
	// Add textures for game elements
	SDL_Texture* loadTexture(const std::string& path);
	void renderSlider();
	void renderText(const std::string& text, SDL_Rect& rect, int size);
	void measureText(const std::string& text, SDL_Rect& rect, int size);
	void renderActions();
	void renderPerso();
	void renderChoppingPlate();
	void renderCookingPot();
	void renderScore();
	void drawEllipse(SDL_Rect ellipse);

private:
	Solution *_sol;
	std::string _pathToExe;

	SDL_Window* window;
	SDL_Renderer* renderer;
	SDL_Texture* background;
	SDL_Texture* perso;
	SDL_Texture* play;
	SDL_Texture* pause;
	SDL_Texture* reload;
	SDL_Texture* onion;
	SDL_Texture* choppedOnion;
	SDL_Texture* plate;

	bool playing = false;
	unsigned long lastFrame = 0;

	Slider slider;

	SDL_Texture* font24Map;
	int font24Width;
	int font24Height;

	SDL_Texture* font18Map;
	int font18Width;
	int font18Height;

	std::vector<int> actionsEndHeight;
	int actionsScroll = 0;
	int actionsTotalHeight = 0;

	const int backgroundWidth = 1265;
	const int backgroundHeight = 784;
	const int persoWidth = 109;
	const int persoHeight = 101;
	const int actionsColWidth = 200;
	const int buttonsSize = 28;
	const int nbWidth = 4*16;

	const int topLeftx = 296;
	const int topLefty = 184;
	const int topRightx = 935;
	const int topRighty = 184;
	const int bottomLeftx = 220;
	const int bottomLefty = 580;
	const int bottomRightx = 1002;
	const int bottomRighty = 580;
	const int scorex = 60;
	const int scorey = 690;
	const int timex = 1180;
	const int timey = 703;

	//grille 11*7
	const int gridWidth = 11;
	const int gridHeight = 7;

};

#endif // DISPLAY_H
